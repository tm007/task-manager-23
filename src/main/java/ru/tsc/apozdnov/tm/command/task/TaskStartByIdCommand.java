package ru.tsc.apozdnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** START TASK BY ID ****");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().changeTaskStatusById(getUserId(), id, Status.IN_PROGRESS);
    }

}
