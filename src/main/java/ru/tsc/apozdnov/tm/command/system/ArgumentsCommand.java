package ru.tsc.apozdnov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentsCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Show argument list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommand();
        for (final AbstractCommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
