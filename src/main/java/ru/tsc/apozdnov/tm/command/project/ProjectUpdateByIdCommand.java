package ru.tsc.apozdnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-update-by-id";

    @NotNull
    public static final String DESCRIPTION = "Update project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** UPDATE PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        serviceLocator.getProjectService().updateById(userId, id, name, description);
    }

}
