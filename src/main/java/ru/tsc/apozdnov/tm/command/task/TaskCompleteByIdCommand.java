package ru.tsc.apozdnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-complete-by-id";

    public static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** COMPLETE TASK BY ID ****");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        serviceLocator.getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
    }

}
