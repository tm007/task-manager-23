package ru.tsc.apozdnov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class VersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Display version application tm.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("1.23.0");
    }

}
