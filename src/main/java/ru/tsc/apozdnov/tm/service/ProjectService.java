package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;

import ru.tsc.apozdnov.tm.api.service.IProjectService;

import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.DescriptionEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IndexIncorrectException;
import ru.tsc.apozdnov.tm.exception.field.NameEmptyException;
import ru.tsc.apozdnov.tm.exception.user.UserIdEmptyException;
import ru.tsc.apozdnov.tm.model.Project;

import java.util.*;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project updateByIndex(@NotNull final String userId, @NotNull Integer index, @NotNull String name, @NotNull String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = Optional.ofNullable(findOneByIndex(index)).orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescriprion(description);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(@NotNull final String userId, @NotNull String id, @NotNull String name, @NotNull String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = Optional.ofNullable(findOneById(id)).orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescriprion(description);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusById(@NotNull final String userId, @NotNull String id, @NotNull Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = Optional.ofNullable(findOneById(userId, id)).orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusByIndex(@NotNull final String userId, @NotNull Integer index, @NotNull Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Project project = Optional.ofNullable(findOneByIndex(index)).orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description, @Nullable final Date dateBegin, @Nullable final Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Project project = Optional.ofNullable(create(userId, name, description)).orElseThrow(ProjectNotFoundException::new);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public @NotNull Project add(@NotNull final String userId, @NotNull Project project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        return repository.add(project);
    }

    @Override
    public @NotNull List<Project> findAll(@NotNull final String userId, final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public void clear(final @NotNull String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear();
    }

}
