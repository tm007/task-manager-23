package ru.tsc.apozdnov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable RoleType role);

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@NotNull String id);

    @Nullable
    User findOneByLogin(@NotNull String login);

    @Nullable
    User findOneByEmail(@Nullable String email);

    @NotNull
    User remove(@NotNull User user);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}
