package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.enumerated.Status;

import ru.tsc.apozdnov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    @NotNull
    Task updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    Task updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, String description);

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    Task changeTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void clear(@NotNull String userId);

}
