package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info(@Nullable String messsage);

    void command(@Nullable String messsage);

    void error(@Nullable Exception ex);

    void debug(@Nullable String message);

}
