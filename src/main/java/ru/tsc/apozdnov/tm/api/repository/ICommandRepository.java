package ru.tsc.apozdnov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    void add(@NotNull AbstractCommand abstractCommand);

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByArgument(@NotNull String arg);

}
