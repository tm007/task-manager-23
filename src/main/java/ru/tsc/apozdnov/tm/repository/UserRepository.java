package ru.tsc.apozdnov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.repository.IUserRepository;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.util.HashUtil;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRoleType(RoleType.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User findOneByLogin(@NotNull final String login) {
        return models.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, final RoleType role) {
        final User user = create(login, password);
        if (role != null) user.setRoleType(role);
        return user;
    }

    @NotNull
    @Override
    public User findOneByEmail(final String email) {
        return models.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return models.stream().anyMatch(user -> login.equals(user.getLogin()));
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return models.stream().anyMatch(user -> email.equals(user.getEmail()));
    }

}
