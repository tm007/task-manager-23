package ru.tsc.apozdnov.tm.repository;

import org.jetbrains.annotations.NotNull;

import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.repository.ICommandRepository;
import ru.tsc.apozdnov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return mapByName.values();
    }

    @NotNull
    @Override
    public void add(@NotNull final AbstractCommand abstractCommand) {
        if (abstractCommand == null) return;
        final String name = abstractCommand.getName();
        if (name != null && !name.isEmpty()) mapByName.put(name, abstractCommand);
        final String arg = abstractCommand.getArgument();
        if (arg != null && !arg.isEmpty()) mapByArgument.put(arg, abstractCommand);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        if (name == null && name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@NotNull final String arg) {
        if (arg == null && arg.isEmpty()) return null;
        return mapByArgument.get(arg);
    }

}


